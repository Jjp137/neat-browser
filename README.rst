Neat Browser
============

An experimental web browser written in Nim using the WebKitGTK engine.

Since WebKitGTK only works on Linux, this browser is restricted to Linux
as well.

Do not expect this project to be worked on very often. Also, it goes without
saying that this browser should not be used with untrusted content.

Building
--------

Install the latest version of the WebKitGTK engine that is available using your
distro's package manager (if any, or else you get to compile it yourself).

On Debian-based distros, you can do this with:

``apt-get install libwebkit2gtk-4.0-37``

Also, the ``oldgtk3`` Nimble package must be installed first with:

``nimble install oldgtk3@#head``

After that, just build main.nim:

``nim c -d:release -o:neat main.nim``

Using
-----

Currently, Neat has a very minimal UI that utilizes WebKit's default context
(right-click) menus.

When entering a URL, the protocol (such as ``https://``) is not inferred
automatically and must be included by the user. This will change if future
versions are released.

To go back and forward through the history, right-click anywhere on the current
page that is not a link to bring up those options.

License
-------

This project is licensed under the GNU General Public License, version 3.
See the COPYING file for details.

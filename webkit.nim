# Copyright (C) 2019 Jjp137 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import oldgtk3/[gtk, gobject, glib]

const lib = "libwebkit2gtk-4.0.so(|.37)"

type
  WebView* = ptr WebViewObj
  WebViewPtr* = ptr WebViewObj
  WebViewObj* = object of ContainerObj

proc webViewGetType*(): GType
  {.cdecl, dynlib: lib, importc: "webkit_web_view_get_type".}

template typeWebView*(): untyped =
  (webViewGetType())

template webView*(obj: untyped): untyped =
  (gTypeCheckInstanceCast(obj, typeWebView, WebViewObj))

proc newWebView*(): WebView
  {.cdecl, dynlib: lib, importc: "webkit_web_view_new".}

proc loadUri*(web_view: WebView, uri: cstring)
  {.cdecl, dynlib: lib, importc: "webkit_web_view_load_uri".}

proc getEstimatedLoadProgress*(web_view: WebView): Gdouble
  {.cdecl, dynlib: lib, importc: "webkit_web_view_get_estimated_load_progress".}

proc estimatedLoadProgress*(web_view: WebView): Gdouble
  {.cdecl, dynlib: lib, importc: "webkit_web_view_get_estimated_load_progress".}

proc getUri*(web_view: WebView): cstring
  {.cdecl, dynlib: lib, importc: "webkit_web_view_get_uri".}

proc uri*(web_view: WebView): cstring
  {.cdecl, dynlib: lib, importc: "webkit_web_view_get_uri".}

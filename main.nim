# Copyright (C) 2019 Jjp137 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import macros
import os
import parseopt

import oldgtk3/[gtk, gobject, glib]
import webkit

proc onActivate(textBox: EntryPtr, webview: WebViewPtr) =
  webview.loadUri(textBox.getText())

proc onDestroy(widget, window: WidgetPtr) =
  gtk.mainQuit()

proc onCloseWebView(webview: WebViewPtr, window: WidgetPtr): bool =
  window.destroy()
  result = true

proc updateProgress(webview: WebViewPtr, spec: GParamSpec,
                    progress: ProgressBarPtr) =
  # The expected callback function for notify signals in C is:
  # void user_function (GObject*, GParamSpec*, gpointer) { ... }
  #
  # A GParamSpec only has metadata so it's basically useless here,
  # so we still need to get the load progress from the WebView itself.
  progress.fraction = webview.getEstimatedLoadProgress()

proc updateTextBoxWithUri(webview: WebViewPtr, spec: GParamSpec,
                          textBox: EntryPtr) =
  textBox.text = webview.getUri()

proc showAbout(helpItem: MenuItemPtr, window: Window) =
  const license_text = staticRead(getProjectPath()/"COPYING")

  showAboutDialog(window, "program-name", "Neat Browser",
                          "license", license_text)

proc createMenuBar(window: Window): MenuBarPtr =
  result = newMenuBar()

  var helpItem = newMenuItemWithLabel("Help")
  result.append(helpItem)

  var helpMenu = newMenu()
  var aboutItem = newMenuItemWithLabel("About")
  helpMenu.append(aboutItem)
  helpItem.setSubMenu(helpMenu)

  discard aboutItem.gSignalConnect("activate", gCallback(showAbout), window)

proc main(debug = false) =
  initWithArgv()

  var window = newWindow(WindowType.TOPLEVEL)
  window.setDefaultSize(800, 600)
  window.setTitle("Neat Browser 0.1.0")

  var grid = newGrid()
  window.add(grid)

  var row: cint = 0

  var menuBar = createMenuBar(window)
  grid.attach(menuBar, 0, row, 1, 1)
  inc row

  var textBox = newEntry()
  textBox.placeholderText = "Enter a URL here..."
  grid.attach(textBox, 0, row, 1, 1)
  inc row

  var webview = newWebView()
  grid.attach(webview, 0, row, 1, 1)
  inc row

  discard webview.gSignalConnect("notify::uri", gCallback(updateTextBoxWithUri),
                                 textBox)
  discard textBox.gSignalConnect("activate", gCallback(onActivate), webview)

  # For some reason, when placed in a grid, the WebView widget is 1x1
  # by default, so tell it to use as much space as possible
  webview.vexpand = true
  webview.hexpand = true

  var barGrid = newGrid()
  grid.attach(barGrid, 0, row, 1, 1)

  var status = newStatusbar()
  status.hexpand = true

  var statusBox = status.getMessageArea()
  statusBox.marginTop = 0
  statusBox.marginBottom = 0
  statusBox.marginStart = 0
  statusBox.marginEnd = 0

  var id = status.push(0, "Welcome to Neat 0.1.0!")
  barGrid.attach(status, 0, 0, 3, 1)

  var progress = newProgressBar()
  progress.valign = Align.CENTER
  # Add some space between the right side of the bar and the window's edge
  progress.marginEnd = 10

  barGrid.attachNextTo(progress, status, PositionType.RIGHT, 1, 1)

  discard webView.gSignalConnect("notify::estimated-load-progress",
                                 gCallback(updateProgress), progress)

  discard window.gSignalConnect("destroy", gCallback(onDestroy), nil)
  discard webview.gSignalConnect("close", gCallback(onCloseWebView), window)

  webview.loadUri("about:blank")

  webview.grab_focus()
  window.showAll()

  if debug:
    windowSetInteractiveDebugging(true)

  gtk.main()

when isMainModule:
  var debug = false

  for kind, key, val in getopt():
    case kind
    of cmdLongOption:
      case key
      of "debug":
        debug = true
      else: discard
    else: discard

  main(debug)
